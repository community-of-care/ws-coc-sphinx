(intro)=
# Einleitung

Im Kapitel [**Community of Care** (kurz **CoC**)](coc)
beschreiben wir die Idee und was wir damit erreichen wollen.

In den Geschichten
[Zukunft](story-future)
und
[Re-Generation](story-re-gen)
beschreiben wir unseren Weg zur **CoC** und welchen Nutzen wir in ihr fuer uns und unsere Gesellschaft sehen.

*TODO:* Introduce other chapters!

Den Quell-Text zu dieser Ausarbeitung findest du in diesem
[Git-Repository](https://codeberg.org/community-of-care/ws-coc-sphinx).
Dort es auch der
[Lizenz-Text](https://codeberg.org/community-of-care/ws-coc-sphinx/src/branch/main/LICENSE)
enthalten.
Mit Hilfe vom Dokumentations-Generator
[SPHINX](https://www.sphinx-doc.org)
wandeln wir den Quell-Text in die Webseite um, welche du in diesem Moment liest.
Auf die gleiche Weise wandeln wir den Quell-Text in eine PDF-Datei um, welche du per klick auf diese
[Auswahl](https://nc.swingbe.de/index.php/s/HQFrbZKqqQTP4r2)
oder durch Kopie der folgenden Adresse herunterladen kannst.

```
https://nc.swingbe.de/index.php/s/HQFrbZKqqQTP4r2
```
