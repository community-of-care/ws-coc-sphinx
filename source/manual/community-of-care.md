(coc)=
# Community of Care

In einer
[Community of Care (kurz CoC)](https://community-of-care.org/)
fokussieren sich Menschen auf gegenseitige Hilfe und Unterstuetzung.
Durch einen umfangreichen Austausch von Beduerfnissen,
Erfahrungen und Lebens-Situationen,
entsteht eine Gemeinschaft mit hoher Verbundenheit.

Viele Kommunikations-Wege sind denkbar,
um diese Verbundenheit zu erziehlen.
Zum Teil ist die Wahl der Kommunikations-Wege von der Lebens-Situation der beteiligten Menschen abhaengig.

Bei hoher geographischer Distanz oder um Mobilitaets-Hindernisse zu umgehen,
kann diese Verbundenheit erziehlt werden, indem sich Menschen mittels digitaler Hilfsmittel austauschen.

Ein mögliches Hilfsmittel ist der so genannte Offers and Needs Market
(*TODO:* Quelle?)
oder kurz OaN Market.

*TODO:* Tbc! The OaN market is not the only tool!

*TODO:* Is the following section outdated?

## Verbundenheit

*TODO:* What are the methods to obtain profound/deep connection?

## Generalisierte Gegenseitigkeit

Unabhaengig von den Kommunikations-Wegen ist die so genannte Generalisierte Gegenseitigkeit (oder Reziprozität) eine Methode,
um die Menschen einer CoC miteinander zu verbinden.

Quellen:
* [Generalisierte Reziprozitaet](https://de.wikipedia.org/wiki/Reziprozit%C3%A4t_(Soziologie)#Generalisierte_Reziprozit%C3%A4t)
* [Paying It Forward](https://en.wikipedia.org/wiki/Pay_it_forward)
* [Paying It Forward Paradox](https://youtu.be/PwfSvQkeRXA?si=u2vnLkngWO2AzjPu)
* [Paying It Forward Another Way](https://www.youtube.com/watch?v=TiTvzgqwbH0)

Die Generalisierte Gegenseitigkeit ist eng verwandt mit der Gegenseitigen Hilfe.

Quellen:
* [de wiki](https://de.m.wikipedia.org/wiki/Gegenseitige_Hilfe)
* [en wiki](https://en.m.wikipedia.org/wiki/Mutual_aid_(organization_theory))

### Reciprocity Ring

Wenn Menschen einer Gemeinschaft die sich gegenseitig fördert,
kann ein Reciprocity Ring oder auch Reciprocity Cycles entstehen.
Ein Reciprocity Ring kann wie folgt beschrieben werden.

* A process of asking forward and giving help
* Make a request for something that you need
* Most of the time focusing on other people and helping with their request

### Reciprocity Paradox

* Generosity isn't the problem.
* Getting people to ask for what they need is the problem.
* The request is the key to the giving and receiving cycle.
kk