(digital-tools)=
# Digitale Hilfsmittel

Wir leben in einem digitalen Zeitalter.
Digitale Dienste und Produkte sind allgegenwärtig.
Der Einfachheit halber nennen wir sie hier digitale Tools.

Wir haben uns so an die digitalen Tools gewöhnt,
dass sie zu unserem Alltags-Begleiter oder persönlichem digitalen Stellvertreter geworden sind.
Manche Menschen gehen noch weiter und identifizieren sich über diese Tools.
Manche Menschen sind so abhängig von diesen Tools,
dass sich das ursprüngliche Ziel (Hilfe) in das Gegenteil (Schaden) umkehrt.

Nicht alle digitalen Tools,
erst recht nicht ihre lange Liste an Funktionen,
sind gleichzeitig auch digitale Hilfsmittel.
Die Mehrheit der Funktionen wurden von den Anbietern in erster Linie geschaffen,
um Gewinn und Wachstum von ihrem Unternehmen zu steigern.
In wie weit eine Funktion einem Menschen diehnt oder nützt,
spielt dabei eine untergeordnete Rolle.
**Hier nicht!**
Wir verstehen das Wort Hilfsmittel wortwoertlich.
Ein digitales Hilfsmittel verstehen wir als Mittel,
welches dem Wohlbefinden vom bedienenden Menschen dient oder hilft.
Wir erstehen es auch als digitales Werkzeug,
weil ein Zeug uns in die Lage versetzt ein Werk zu schaffen.

*TODO:* Add the self-critical and sufficient aspekt of digital tools!

*TODO:* Add digital tools study and conclusion!

*TODO:* Add focus aspect of digital tools! For commerce/profit vs for community?

*TODO:* Add notes coc-tools!

*TODO:* Is the following section outdated?

## Offers and Needs Market

Der Offers and Needs (OaN) Market soll als digitales Hilfsmittel die Menschen einer Region bessere Verbinden, um generalisierte Gegenseitigkeit zu fördern.

### Anforderungen

* Menschen sollen über einen simplen Weg "Bedürfnisse" austauschen können.
* Weil dieser Austausch teilweise kurze geographische Entfernungen/Wege bedingt, soll die Region berücksichtigt sein.
* Connect people
* KISS

### Prototypen

* https://discuss.swingbe.de
