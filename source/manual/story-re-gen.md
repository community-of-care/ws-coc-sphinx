(story-re-gen)=
# Geschichte: Re-Generation

Aus Gründen wollen wir unsere Gesellschaft in einer freien und gerechten Weise organisieren wie es bspw. die Demokratie oder Soziokratie vorsieht.
Daneben streben wir eine Lebensweise an,
welche im Einklang mit Mensch, Natur und unseren Werten steht.
Wir schlussfolgern,
dass wir diese Ziele erreichen,
wenn wir als Gesellschaft eine Lebensweise innerhalb der planetaren Grenzen fuehren.
Mit anderen Worten:
Uns ist wichtig,
die gegebenen jährliche Ressourcen zu (wert-)schätzen und sie zu (be-)nutzen ohne sie zu (ver-)nutzen oder sogar Schulden zu machen.
Uns und alle gleich gesinnten Menschen koennen wir also als regenerative Generation oder kurz Re-Generation (Re-Gen) bezeichnen.

Als Re-Generation stehen wir im Alltag vor schwer passierbaren Hindernissen.

Beispiele:
* Gefuehlt vergeht kaum eine Woche, in der wir nicht fast zum Konsum von Dingen, auf Grund von gesellschaftlichen Zwaengen, genoetigt werden.
* Viele Konsum-Gegenstände kommen aus entfernten Orten, dessen Eigenschaften, Entstehungs-Bedingungen oder Kontext wir aus der ferne nicht beurteilen koennen.
* Wenn wir bei globalen Konzernen konsumieren, profitieren in erster Linie deren Anleger, aber nicht wir vor Ort in unserer Region.

TODO:* Add proper/more precise examples!

Das Finanz- und Wirtschafts-System aber auch teilweise heutige Normen und Werte stehen den Zielen der Re-Generation wie Hindernisse im Weg.
Der daraus resultierende hohe Wiederstand kostet der Re-Generation im Alltag viel Energie und rueckt das Erreichen ihrer Ziele in eine schlecht sichtbare Zukunft.
Dieser Wiederspruch frustriert.

Trotz oder wegen dieser Situation kommen wir zum Fazit,
dass bereits im heutigen Alltag eine Lebensweise im Einklang mit den Zielen der Re-Generation machbar ist.
Wir möchten (er-)leben,
was uns und die Re-Generation in die Lage versetzen den angesprochenen Wiederspruch im heutigen Alltag aufzulösen.
Eine Möglichkeit stellt die so genannte
[Community of Care](coc)
dar.
