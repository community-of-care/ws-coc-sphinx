(story-future)=
# Geschichte: Zukunft

Wer hat Lust bereits heute in einer Zukunft zu leben, welche wir durch Transformation erst in einer schwer sichtbaren 
und zeitlich weit entfernten Zukunft erreichen?

## Heute (gegeben)

Die heutige Lebensweise verkauft uns eine Geschichte welche in der Kurzfassung wie folgt lautet:

Nur wenn wir viel arbeiten, erzielen wir einen Verdienst,
welcher uns einen hohen Wohlstand sichert.
Geld verwenden wir nich mehr als Tauschmittel.
Es ist fuer uns zur einzigen Ressource geworden, welche eine Bedeutung fuer uns hat und uns unbegrenzten Wohlstand ermoeglicht.
Diese Denkweise erscheint uns sowohl einfach als auch logisch.
Solange die "*Kohle*" stimmt, brauchen wir uns keine Sorgen machen.
Um den Schein einer sozialen Marktwirtschaft aufrecht zu erhalten,
welche keine Menschen abhaengt und sich selbst regelt,
versuchen die Angestellten sich mit Solidar-Systemen über Wasser zu halten,
während ein previligierter Teil wie Dagobert Duck im Geld badet.

## Morgen (gesucht)

Wir stellen uns eine Zukunft vor,
in der Menschen und ihre Verbundenheit im Vordergrund stehen und unsere Gesellschaft sich die Pflege der sich daraus resultierenden Gemeinschaften zum Zweck macht.
Aus der gegenseitigen Hilfe und Untertützung resultiert eine Verbundenheit,
aus der wir unser Wohlbefinden und unseren Wohlstand definieren und schöpfen.

**Beispiel:** Statt einfach weiter Autos zu verkaufen,
welche uns die Erde und Luft zum atmen nehmen,
widmet sich die Automobilindustrie dem Angebot von suffizienten Mobilitäts-Dienstleistungen für das gesamte Spektrum der Gesellschaft,
so das Mobilität nicht in den Selbsterhaltung/Selbstzweck sondern in die gesellschaftliche Teihabe einzahlt.

## CoC (Lösungs-Möglichkeit)

Durch regenerative Transformation wird die oben beschriebene Zukunft irgendwie, irgendwo, irgendwann Wirklichkeit werden.
Dieser Ausblick bringt uns allerdings nicht unbedingt Zufriedenheit im heutigen Alltagsleben.

Eine Möglichkeit die beschriebene Zukunft hier und jetzt zu (er-)leben ist die so genannte
[Community of Care](coc)
.
