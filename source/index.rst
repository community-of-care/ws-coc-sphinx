.. getting-started documentation master file, created by
   sphinx-quickstart on Thu Apr 13 17:31:40 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Community of Care
=================

.. toctree::
   :numbered:
   :maxdepth: 1
   :caption: Contents:

   intro
   manual/community-of-care
   manual/story-future
   manual/story-re-gen

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
