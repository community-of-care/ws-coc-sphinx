# General

This file describes how to convert my thoughts about the
**Community of Care**
into a PDF file and a static website using a documentation generator.

# Introduction

* setup the project
```
sphinx-quickstart
```

* install Sphinx
```
https://www.sphinx-doc.org/en/master/usage/installation.html
```

* get started with Sphinx
```
https://www.sphinx-doc.org/en/master/usage/quickstart.html#
```

# Build Documentation

## Python Dependencies

In order to generate the documentation,
first create a Python virtual environment using the
`venv` module included in Python like this.

* Create a virtual Python environment in the directory `venv`.
```
python3 -m venv venv
```

* Activate the virtual Python environment.
```
source venv/bin/activate
```

* Install the packages listed in [requirements.txt](./requirements.txt) using `pip`.
```
pip3 install -r requirements.txt
```

## Running Generator via Make

Execute make without an argument to see which targets are available
```
make
```

The top level directory contains a Makefile.
Generate HTML output
```
make html
```

Generate PDF output
```
make latexpdf
```

The output files can be found in the subfolder `./build`.

All generated intermediate artifacts and the final output files can be found in the `./build` subfolder.

Generate all output
```
make bundle
```

### Clean up

1. Deactivate the virtual Python environment
```
deactivate
```

2. Clean up the build sources:
```
make clean
```

# File System

* `build`: build directory
* `Makefile`: build configuration
* `readme.md`: this file
* `source`: source directory
# Links

* [getting started](https://www.sphinx-doc.org/en/master/usage/quickstart.html)